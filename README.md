# TicTacToe
TicTacToe Made with my OpenGLWrapper library and OpenGLWrapperUtils library

NOTE: This git repository uses submoduels so to clone it you need to add the arguments: --recurse-submodules -j8 to the git clone command!!!

NOTE: Since this repository uses submodules DO NOT download it using the web interface it does not download submodules instead clone the project with the arguments: --recurse-submodules -j8 !!!

# Compile of executable binary
 
 Note: The automatron commands must be run from the project folder where this README is located!!!

 - Build the release of OpenGLWrapper and OpenGLWrapperUtils, and make sure all the projects including this are in the same directory

 Note: Remove -master from the folder names that the projects are stored in.

  # GNU/Linux (using make to build and the default package manager to install stuff and assuming that the right libraries are installed from the fact that the user has to have the OpenGLWrapper project compiled for which he needs to install the libraries)

   - Ubuntu: ./automatron9000/automatron9000.sh -m debug -o ubuntu
   
   - Arch: ./automatron9000/automatron9000.sh -m debug -o arch

  # Mac OS (using make to build and the brew package manager to install stuff and assuming that the right libraries are installed from the fact that the user has to have the OpenGLWrapper project compiled for which he needs to install the libraries)
   Note: This command will download and install the required applications needed to build and install brew. 

   - ./automatron9000/automatron9000.sh -m debug -o macos

  # Windows (using make to build and the chocolatey package manager to install stuff and using the libraries from the Libraries\windows folder from the OpenGLWrapper project folder)
   Note: This command will download and install the required applications needed to build and install chocolatey.

   - Open cmd as administrator
   - Run: automatron9000\automatron9000.bat -m debug -o windows
   