#ifndef PUBLICVARIABLES_H
#define PUBLICVARIABLES_H

struct PublicVariables{

     static const unsigned int xShapeVerticesPosLen;
     static const float xShapeVerticesPos[];


	 static const unsigned int xShapeIndicesLen;
     static const int xShapeIndices[];

	//----------------------------------------------------------------------------------


     static const unsigned int oShapeVerticesPosLen;
     static const float oShapeVerticesPos[];

     static const unsigned int oShapeIndicesLen;
     static const int oShapeIndices[];

};

#endif
