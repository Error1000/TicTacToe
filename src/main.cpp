#include <OpenGLWrapper/Include.h>
#include <OpenGLWrapperUtils/Include.h>
#include "PublicVariables.h"

//Sleep
#include <chrono>
#include <thread>

//knots and crosses array
#include <vector>


using namespace std;
using namespace UserUtils;

void setup();
void render();
void onExit();
template<size_t lenx, size_t leny> unsigned short checkGrid(const unsigned short (&grid)[lenx][leny]);


int main(){
try{
Window::setWidth(400);
Window::setHeight(400);
Window::setFullScreen(false);
Window::setTitle("X and O!");
Window::useVSync(true);
Wrapper::initWrapper(setup, render, onExit);
}catch(APIError a){
 Logger::log(a.message, Level::Error);
 return 0;
}catch(WindowError e){
 Logger::log(e.message, Level::Error);
 return 0;
}

Wrapper::runWrapper();

}

Shape *x;
Shape *o;
DrawableRectangle *lines;
glm::mat3 projection;
unsigned short gameGrid[3][3];
unsigned int sqw, sqh;
vector<DrawableObject2D*> objects;



void setup(){
try{
DefaultShader2D::init();
}catch(ShaderError s){
 Logger::log(s.message, Level::Error);
 return;
}

backgroundColor(255, 255, 255);

for(unsigned int i = 0; i < 3; i++)
    for(unsigned int j = 0; j < 3; j++)
      gameGrid[i][j] = 0;

x = new Shape(PublicVariables::xShapeVerticesPos, PublicVariables::xShapeVerticesPosLen, DefaultShader2D::getVertPosId(), 2, GL_STATIC_DRAW,
                     PublicVariables::xShapeIndices, PublicVariables::xShapeIndicesLen, GL_STATIC_DRAW);


o = new Shape(PublicVariables::oShapeVerticesPos, PublicVariables::oShapeVerticesPosLen, DefaultShader2D::getVertPosId(), 2, GL_STATIC_DRAW,
                     PublicVariables::oShapeIndices, PublicVariables::oShapeIndicesLen, GL_STATIC_DRAW);


lines = new DrawableRectangle[4]{
    {vec2(10, Window::getHeight()), vec2(-Window::getWidth()/6, 0)},
    {vec2(10, Window::getHeight()), vec2(Window::getWidth()/6, 0)},

    {vec2(Window::getWidth(), 10), vec2(0, Window::getHeight()/6)},
    {vec2(Window::getWidth(), 10), vec2(0, -Window::getHeight()/6)},

};

sqw = Window::getWidth()/3;
sqh = Window::getHeight()/3;

projection = glm::ortho(-(float)Window::getWidth()/2, (float)Window::getWidth()/2, -(float)Window::getHeight()/2, (float)Window::getHeight()/2 );

//Everything uses defaultshader so let's just bind it now and never llok at it again, because performance
DefaultShader2D::bindShader();
DefaultShader2D::useTexture(false);
DefaultShader2D::setRenderingColor(0, 0, 0);

try{
 DefaultShader2D::getShader()->validateShader();
}catch(ShaderError validationError){
 Logger::log(validationError.message, Level::Error);
 Logger::log("OpenGL shader validation error!");
 return;
}

}

bool pressed = false;
bool turn = true;
int exitGame = 0;


void render(){
if(Window::isKeyPressed(GLFW_KEY_ESCAPE)) exitWrapper();

if(Window::isMouseKeyPressed(GLFW_MOUSE_BUTTON_1)){
 if(!pressed){
  const unsigned int sqrw = Window::getMousePos().x/sqw+1;
  const unsigned int sqrh = Window::getMousePos().y/sqh+1;
  const int sqrmx = ((int)(sqrw*sqw)-Window::getWidth()/2)-(int)sqw/2;
  const int sqrmy = ((int)(sqrh*sqh)-Window::getHeight()/2)*-1+(int)sqh/2;

  bool match = false;
  DrawableObject2D *obj = new DrawableObject2D( turn ? x : o , DefaultShader2D::getShader());
  obj->setPosition( vec2(sqrmx, sqrmy) );
  obj->setSize( vec2(sqw/1.34f, sqh/1.34f));
  for(DrawableObject2D *object : objects){
    if(object->getPosition() == obj->getPosition()){
       match = true;
       break;
   }
  }

  if(!match){
    objects.push_back(obj);
    gameGrid[sqrh-1][sqrw-1] = turn + 1;
    turn = !turn;
  }

  pressed = true;
 }

}else{
 pressed = false;
}


for(unsigned int i = 0; i < 4; i++){
lines[i].bindObjectShape();
DefaultShader2D::getShader()->setMatrixUniform(DefaultShader2D::getTransformationMatrixId(), projection * Camera2D::getViewMatrix() * lines[i].getModelMatrix() );
lines[i].renderObject();
}


for(DrawableObject2D* obj : objects){
obj->bindObjectShape();
DefaultShader2D::getShader()->setMatrixUniform(DefaultShader2D::getTransformationMatrixId(), projection * Camera2D::getViewMatrix() * obj->getModelMatrix());
obj->renderObject();
}


if(exitGame > 3){
    std::this_thread::sleep_for(std::chrono::seconds(4));
    exitWrapper();
}

unsigned short ww = checkGrid(gameGrid);

if(ww != 0){
    if(ww == 1)
        Window::setTitle("0 won!");
    else if(ww == 2)
        Window::setTitle("X won!");
    else if(ww == 3)
        Window::setTitle("It's a draw!");

    //This is here just so we can draw the last x or o before exiting
    exitGame++;
}



}



void onExit(){

delete[] lines;
for(DrawableObject2D *obj : objects) delete obj;
objects.clear();
delete x;
delete o;
DefaultShader2D::destroy();
}





template<size_t lenx, size_t leny> unsigned short checkGrid(const unsigned short (&grid)[lenx][leny]){

//0 = none, 1 = x won, 2 = o won, 3 = draw
unsigned short whowon = 0;

//Check rows
for(unsigned int i = 0; i < lenx; i++){
        bool yes = true;
    for(unsigned int j = 0; j < leny-1; j++){
        if(grid[i][j] != grid[i][j+1] && grid[i][j] != 0 && grid[i][j+1] != 0)yes = false;
        if(grid[i][j] == 0 || grid[i][j+1] == 0)yes = false;
        if(grid[i][j] != grid[i][j+1])yes = false;
        if(!yes)break;
    }

    if(yes){ whowon = grid[i][0]; return whowon; }
}

//Check columns
for(unsigned int i = 0; i < lenx; i++){
    bool yes = true;
    for(unsigned int j = 0; j < leny-1; j++){
        if(grid[j][i] != grid[j+1][i] && grid[j][i] != 0 && grid[j][i+1] != 0)yes = false;
        if(grid[j][i] == 0 || grid[j+1][i] == 0)yes = false;
        if(grid[j][i] != grid[j+1][i])yes = false;
        if(!yes)break;
    }
    if(yes){ whowon = grid[0][i]; return whowon; }
}

//Check diagonals
unsigned int j = 0, i = 0;
bool yes = true;

for(i = 0; i < lenx-1; i++){

        if(grid[i][j] != grid[i+1][j+1] && grid[i][j] != 0 && grid[i+1][j+1] != 0)yes = false;
        if(grid[i][j] == 0 || grid[i+1][j+1] == 0)yes = false;
        if(grid[i][j] != grid[i+1][j+1])yes = false;
        if(!yes)break;

    if(j < leny-1)j++;
}
if(yes){ whowon = grid[0][0]; return whowon; }

j = leny-1;
yes = true;
for(i = 0; i < lenx-1; i++){

        if(grid[i][j] != grid[i+1][j-1] && grid[i][j] != 0 && grid[i+1][j-1] != 0)yes = false;
        if(grid[i][j] == 0 || grid[i+1][j-1] == 0)yes = false;
        if(grid[i][j] != grid[i+1][j-1])yes = false;
        if(!yes)break;

    if(j > 0)j--;
}
if(yes){ whowon = grid[0][leny-1]; return whowon; }

//Check for a draw
bool draw = true;
for(i = 0; i < lenx; i++)
    for(j = 0; j < leny; j++)
        if(gameGrid[i][j] == 0){draw = false; break;}

if(draw)return 3;

return 0;
}
